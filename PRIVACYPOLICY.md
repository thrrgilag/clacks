# Privacy Policy

### Matrix client
* Clacks is a Matrix protocol client and is compatible with all Matrix servers and Matrix identity servers. All communications conducted while using Clacks use a Matrix server and the Matrix identity server.
* The default server in Clacks is https://matrix.org and the default identity server is https://vector.im.
* Clacks doesn't operate any server or remote service.
 * All communication of substantive content between Clacks and any server is done in secure way, using transport encryption to protect it. End-to-end encryption will follow.
 * Clacks is not responsible for the data processing carried out by any Matrix server or Matrix identity server.
 * Clacks offers the option to use phone numbers to find contacts. This is not a requirement of normal operation and is intended only as a convenience. No contact details are uploaded without user interaction! The user can choose to upload all or some or no contacts to the identity server!

### Push Notifications

* The Matrix server selected by the user will automatically send push notifications to the UBports push service. These notifications are encrypted with the https protocol between the device and the Matrix server and on to the official UBports Matrix gateway at https://push.ubports.com:5003/_matrix/push/r0/notify This server forwards the notification to UBports push service at https://push.ubports.com and then sends this on as a push notification to the user's device(s). 
