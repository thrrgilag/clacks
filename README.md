# Clacks

a matrix client for Ubuntu Touch

## Features
 * Single and group chats
 * Send images and files
 * ContentHub integration
 * Offline chat history
 * Push Notifications
 * Account settings
 * Display user avatars
 * Themes, chat wallpapers and dark mode
 * Device management
 * Edit chat settings and permissions
 * Kick, ban and unban users
 * Display and edit chat topics
 * Change chat&user avatars
 * Archived chats
 * Display communities
 * User status (presences)
 * Display contacts and find contacts with their phone number or email address
 * Discover public chats on the user's homeserver
 * Registration (currently only working with ubports.chat and NOT with matrix.org due captchas)

#### How to build

1. Clone this repo:
```
git clone --recurse-submodules https://gitlab.com/thrrgilag/clacks
cd clacks
```

##### Build Click for Ubuntu Touch

2. Install clickable as described here: https://gitlab.com/clickable/clickable

3. Build with clickable
  * armhf
```
clickable build --arch armhf
```
  * arm64
```
clickable build --arch arm64
```

## Contributing

You can open issues and merge requests to this project on [GitLab]. For general discussion please join the public matrix room at [#clacks:matrix.org].


## Credits

A very special thanks Krille Fear for creating the much loved original [FluffyChat] for Ubuntu Touch.


[GitLab]: https://gitlab.com/thrrgilag/clacks
[#clacks:matrix.org]: https://matrix.to/#/#clacks:matrix.org
[FluffyChat]: https://gitlab.com/KrilleFear/fluffychat
