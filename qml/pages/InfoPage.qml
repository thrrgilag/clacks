import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import "../components"

Page {
    id: infoPage
    anchors.fill: parent

    header: PageHeader {
        title: i18n.tr('Clacks %1 on %2').arg(version).arg(platform)
    }


    ScrollView {
        id: scrollView
        width: parent.width
        height: parent.height - header.height
        anchors.top: header.bottom
        contentItem: Column {
            width: infoPage.width

            Image {
                id: coffeeImage
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: parent.width / 4
                width: parent.width / 2
                height: width
                source: "../../assets/clacks-symbolic.png"
            }

            SettingsListLink {
                name: i18n.tr("Privacy Policy")
                icon: "private-browsing"
                page: "PrivacyPolicyPage"
                sourcePage: infoPage
            }

            SettingsListItem {
                name: i18n.tr("Contributors")
                icon: "contact-group"
                onClicked: Qt.openUrlExternally("https://gitlab.com/thrrgilag/clacks/graphs/master")
            }

            SettingsListItem {
                name: i18n.tr("Source code")
                icon: "text-xml-symbolic"
                onClicked: Qt.openUrlExternally("https://gitlab.com/thrrgilag/clacks")
            }

            SettingsListItem {
                name: i18n.tr("License")
                icon: "x-office-document-symbolic"
                onClicked: Qt.openUrlExternally("https://gitlab.com/thrrgilag/clacks/blob/master/LICENSE")
            }

        }
    }

}
